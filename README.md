<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><a href="https://peterdsharpe.github.io/AeroSandbox/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">航空沙盒</font></font></a> <g-emoji class="g-emoji" alias="airplane"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✈️</font></font></g-emoji></h1><a id="user-content-aerosandbox-airplane" class="anchor" aria-label="永久链接：AeroSandbox：飞机：" href="#aerosandbox-airplane"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">作者：</font></font><a href="https://peterdsharpe.github.io" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Peter Sharpe</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（&lt;pds [at] mit [dot] edu&gt;）</font></font></p>
<p dir="auto"><a href="https://pepy.tech/project/aerosandbox" rel="nofollow"><img src="https://camo.githubusercontent.com/6a75618aef976f8973d8faa85be05f56491bbd3a3c7da06324a0b3a7fe109de5/68747470733a2f2f706570792e746563682f62616467652f6165726f73616e64626f78" alt="下载" data-canonical-src="https://pepy.tech/badge/aerosandbox" style="max-width: 100%;"></a>
<a href="https://pepy.tech/project/aerosandbox" rel="nofollow"><img src="https://camo.githubusercontent.com/b3d6dd1a4dc98f8488d2d62e726b137900f20fd82769314808baee1d29cbca06/68747470733a2f2f706570792e746563682f62616467652f6165726f73616e64626f782f6d6f6e7468" alt="每月下载量" data-canonical-src="https://pepy.tech/badge/aerosandbox/month" style="max-width: 100%;"></a>
<a href="https://github.com/peterdsharpe/AeroSandbox/actions/workflows/run-pytest.yml"><img src="https://github.com/peterdsharpe/AeroSandbox/workflows/Tests/badge.svg" alt="构建状态" style="max-width: 100%;"></a>
<a href="https://pypi.python.org/pypi/aerosandbox" rel="nofollow"><img src="https://camo.githubusercontent.com/1a50cdfffc0a79c0149accf01fa19701035148b1c8516a008b0733323f6a824c/68747470733a2f2f696d672e736869656c64732e696f2f707970692f762f6165726f73616e64626f782e737667" alt="皮伊" data-canonical-src="https://img.shields.io/pypi/v/aerosandbox.svg" style="max-width: 100%;"></a>
<a href="https://aerosandbox.readthedocs.io/en/master/?badge=master" rel="nofollow"><img src="https://camo.githubusercontent.com/2f63191af75d1f7f2f2682a13bf9eb99ffccb9da993b9672594689fa2ab72bd2/68747470733a2f2f72656164746865646f63732e6f72672f70726f6a656374732f6165726f73616e64626f782f62616467652f3f76657273696f6e3d6d6173746572" alt="文件状态" data-canonical-src="https://readthedocs.org/projects/aerosandbox/badge/?version=master" style="max-width: 100%;"></a>
<a href="https://opensource.org/licenses/MIT" rel="nofollow"><img src="https://camo.githubusercontent.com/1f2b1b279a9ea33c1ce511eb783f850c0bccad73f3a50d92d520166e2834fe00/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4c6963656e73652d4d49542d672e737667" alt="许可证：麻省理工学院" data-canonical-src="https://img.shields.io/badge/License-MIT-g.svg" style="max-width: 100%;"></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AeroSandbox 是一个 Python 包，可帮助您设计和优化飞机和其他工程系统。</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="/peterdsharpe/AeroSandbox/blob/master/aerosandbox/numpy"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从本质上讲，AeroSandbox 是一个优化套件，它将熟悉的 NumPy 语法</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">的易用性与</font></font><a href="/peterdsharpe/AeroSandbox/blob/master/tutorial/10%20-%20Miscellaneous/03%20-%20Resources%20on%20Automatic%20Differentiation.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现代自动微分</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">的强大功能结合在一起</font><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这种自动微分极大地提高了大型问题的优化性能：</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">具有数万个决策变量的设计问题在笔记本电脑上只需几秒钟即可解决</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。 AeroSandbox 还附带数十个端到端可微分的航空物理模型，使您可以</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">同时优化飞机的空气动力学、结构、推进、任务轨迹、稳定性等。</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保持 AeroSandbox 易于学习和使用是首要任务。</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">复杂性是可选的</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- 您可以在有帮助的情况下使用 AeroSandbox 的内置物理模型，也可以添加您自己的任意自定义物理模型。</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>pip install aerosandbox[full]
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="pip install aerosandbox[full]" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<hr>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我可以使用 AeroSandbox 做什么？</font></font></h3><a id="user-content-what-can-i-do-with-aerosandbox" class="anchor" aria-label="永久链接：我可以使用 AeroSandbox 做什么？" href="#what-can-i-do-with-aerosandbox"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 AeroSandbox 设计和优化整架飞机：</font></font></p>
<table>
    <tbody><tr>
        <td width="50%" valign="top">
            <p align="center" dir="auto">
                <a href="https://github.com/peterdsharpe/Feather-RC-Glider"><i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Feather</font></font></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（超轻量1米级遥控机动滑翔机）</font></font></a>
            </p>
            <a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/peterdsharpe/Feather-RC-Glider/master/CAD/feather.png"><img src="https://raw.githubusercontent.com/peterdsharpe/Feather-RC-Glider/master/CAD/feather.png" alt="羽毛第一页" style="max-width: 100%;"></a>
        </td>
        <td width="50%" valign="top">
            <p align="center" dir="auto">
                <a href="https://github.com/peterdsharpe/solar-seaplane-preliminary-sizing"><i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SEAWAY-Mini</font></font></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（太阳能电动、翼展 13 英尺的水上飞机）</font></font></a>
            </p>
            <a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/peterdsharpe/solar-seaplane-preliminary-sizing/main/CAD/renders/seaway_mini_packet_Page_1.png"><img src="https://raw.githubusercontent.com/peterdsharpe/solar-seaplane-preliminary-sizing/main/CAD/renders/seaway_mini_packet_Page_1.png" alt="Seaway-Mini首页" style="max-width: 100%;"></a>
        </td>
    </tr>
</tbody></table>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 AeroSandbox 支持现实世界的飞机开发项目，从您的第一个草图到您的首次飞行及以后：</font></font></p>
<table>
    <tbody><tr>
        <td width="50%" valign="top">
            <p align="center" dir="auto">
                <a href="https://github.com/peterdsharpe/DawnDesignTool"><font style="vertical-align: inherit;"></font><i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2020 年春季 AeroSandbox 中的Dawn</font></font></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（用于气候科学研究的太阳能电动飞机）</font><font style="vertical-align: inherit;">的初始概念草图 + 尺寸调整</font></font></a>
            </p>
            <a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/AeroSandbox/blob/master/media/images/dawn1-first-sketch.png"><img src="https://github.com/peterdsharpe/AeroSandbox/raw/master/media/images/dawn1-first-sketch.png" alt="黎明初始设计" style="max-width: 100%;"></a>
        </td>
        <td width="50%" valign="top">
            <p align="center" dir="auto">
                <a href="https://youtu.be/CyTzx9UCvyo" rel="nofollow"><i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Dawn</font></font></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（后更名为</font></font><i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SACOS</font></font></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">）于 2022 年秋季首飞</font></font></a>
            </p>
            <p align="center" dir="auto"><a href="https://www.electra.aero/news/sacos-first-flight" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（Electra.aero 通过出色的工程设计和协调进行了大量的构建工作！）</font></font></a></p>
            <a target="_blank" rel="noopener noreferrer" href="/peterdsharpe/AeroSandbox/blob/master/media/images/SACOS%20First%20Flight%20Zoomed.jpg"><img src="/peterdsharpe/AeroSandbox/raw/master/media/images/SACOS%20First%20Flight%20Zoomed.jpg" alt="SACOS首飞" style="max-width: 100%;"></a>
        </td>
    </tr>
</tbody></table>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 AeroSandbox 探索违反直觉的复杂设计权衡，所有这些都是在概念设计的最早阶段</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，这些见解发挥最大作用</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：</font></font></p>
<table>
	<tbody><tr>
		<td width="33%" valign="top">
			<p align="center" dir="auto">
				<a href="https://github.com/peterdsharpe/DawnDesignTool"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">探索太阳能飞机需要多大才能飞行，作为季节性和纬度的函数</font></font></a>
			</p>
			<a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/DawnDesignTool/raw/master/docs/30kg_payload.svg"><img src="https://github.com/peterdsharpe/DawnDesignTool/raw/master/docs/30kg_payload.svg" alt="黎明季节性纬度贸易空间" style="max-width: 100%;"></a>
		</td>
		<td width="33%" valign="top">
			<p align="center" dir="auto">
				<a href="https://www.popularmechanics.com/military/aviation/a13938789/mit-developing-mach-08-rocket-drone-for-the-air-force/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">探索如果我们添加高度限制，</font></font><i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Firefly</font></font></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> （一款 0.8 马赫的火箭无人机）的任务范围会如何变化，同时优化飞机设计和轨迹</font></font></a>
			</p>
			<a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/AeroSandbox/blob/master/media/images/firefly-range-ceiling-trade.png"><img src="https://github.com/peterdsharpe/AeroSandbox/raw/master/media/images/firefly-range-ceiling-trade.png" alt="萤火虫系列天花板贸易" style="max-width: 100%;"></a>
		</td>
		<td width="33%" valign="top">
			<p align="center" dir="auto">
				<a href="https://github.com/peterdsharpe/transport-aircraft"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">考虑非设计性能，探索航空公司机队需要多少架 LH2 飞机来覆盖市场</font></font></a>
			</p>
			<a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/transport-aircraft/raw/master/figures/lh2_market_segmentation_2.svg"><img src="https://github.com/peterdsharpe/transport-aircraft/raw/master/figures/lh2_market_segmentation_2.svg" alt="LH2 市场覆盖" style="max-width: 100%;"></a>
		</td>
	</tr>
</tbody></table>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 AeroSandbox 作为纯粹的空气动力学工具包：</font></font></p>
<table>
	<tbody><tr>
		<td width="33%" valign="top">
			<p align="center" dir="auto">
				<a href="https://github.com/peterdsharpe/AeroSandbox/blob/master/tutorial/06%20-%20Aerodynamics/01%20-%20AeroSandbox%203D%20Aerodynamics%20Tools/01%20-%20Vortex%20Lattice%20Method/01%20-%20Vortex%20Lattice%20Method.ipynb"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">滑翔机的 VLM 模拟，副翼偏转 +-30°</font></font></a>
			</p>
			<a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/AeroSandbox/blob/master/media/images/vlm3_with_control_surfaces.png"><img src="https://github.com/peterdsharpe/AeroSandbox/raw/master/media/images/vlm3_with_control_surfaces.png" alt="VLM模拟" style="max-width: 100%;"></a>
		</td>
		<td width="33%" valign="top">
			<p align="center" dir="auto">
				<a href="https://github.com/peterdsharpe/AeroSandbox/blob/master/tutorial/06%20-%20Aerodynamics/01%20-%20AeroSandbox%203D%20Aerodynamics%20Tools/01%20-%20Vortex%20Lattice%20Method/01%20-%20Vortex%20Lattice%20Method.ipynb"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用任意目标和约束对机翼平面进行气动形状优化</font></font></a>
			</p>
			<a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/AeroSandbox/blob/master/media/images/wing_optimization.png"><img src="https://github.com/peterdsharpe/AeroSandbox/raw/master/media/images/wing_optimization.png" alt="机翼优化" style="max-width: 100%;"></a>
		</td>
		<td width="33%" valign="top">
			<p align="center" dir="auto">
				<a href="https://github.com/peterdsharpe/AeroSandbox/blob/master/tutorial/06%20-%20Aerodynamics/02%20-%20AeroSandbox%202D%20Aerodynamics%20Tools/02%20-%20NeuralFoil%20Optimization.ipynb"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">根据空气动力学、结构和制造考虑因素优化翼型形状</font></font></a>
			</p>
			<a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/AeroSandbox/blob/master/media/images/airfoil_optimization.png"><img src="https://github.com/peterdsharpe/AeroSandbox/raw/master/media/images/airfoil_optimization.png" alt="翼型优化" style="max-width: 100%;"></a>
		</td>
	</tr>
</tbody></table>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在许多其他学科中：</font></font></p>
<table>
	<tbody><tr>
		<td width="50%" valign="top">
			<p align="center" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
				复合材料管梁的结构优化
			</font></font></p>
			<a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/AeroSandbox/blob/master/media/images/beam-optimization.png"><img src="https://github.com/peterdsharpe/AeroSandbox/raw/master/media/images/beam-optimization.png" alt="光束优化" style="max-width: 100%;"></a>
		</td>
		<td width="50%" valign="top">
			<p align="center" dir="auto">
				<a href="https://github.com/peterdsharpe/AeroSandbox/blob/master/aerosandbox/library/propulsion_electric.py"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">螺旋桨匹配的电机分析</font></font></a>
			</p>
			<a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/AeroSandbox/blob/master/media/images/motor_perf.png"><img src="https://github.com/peterdsharpe/AeroSandbox/raw/master/media/images/motor_perf.png" alt="电机性能" style="max-width: 100%;"></a>
		</td>
	</tr>
	<tr>
		<td>
			<p align="center" valign="top" dir="auto">
				<a href="https://github.com/peterdsharpe/transport-aircraft"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分析非常规推进的工具（例如 LH2）</font></font></a>
			</p>
			<a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/transport-aircraft/raw/master/figures/three_view_annotated.svg"><img src="https://github.com/peterdsharpe/transport-aircraft/raw/master/figures/three_view_annotated.svg" alt="LH2飞机三视图" style="max-width: 100%;"></a>
		</td>
		<td>
			<p align="center" valign="top" dir="auto">
				<a href="https://github.com/peterdsharpe/AeroSandbox/tree/master/aerosandbox/library/weights"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从微型无人机到客机的飞机的详细重量估算</font></font></a>
			</p>
			<a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/transport-aircraft/raw/master/figures/mass_budget.png"><img src="https://github.com/peterdsharpe/transport-aircraft/raw/master/figures/mass_budget.png" alt="大众预算" style="max-width: 100%;"></a>
		</td>
</tr>
</tbody></table>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">轻松地将 AeroSandbox 与所有您喜爱的工具连接：</font></font></p>
<table>
    <tbody><tr>
        <td width="33%" valign="top">
            <p align="center" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                其他概念设计工具（AVL、XFLR5、XFoil、ASWING、MSES 等）
            </font></font></p>
            <a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/AeroSandbox/blob/master/media/images/airfoil_contours.png"><img src="https://github.com/peterdsharpe/AeroSandbox/raw/master/media/images/airfoil_contours.png" alt="X箔" style="max-width: 100%;"></a>
        </td> 
          <td width="33%" valign="top">
                <p align="center" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                    通过 STEP 导出的 CAD 工具（SolidWorks、Fusion 360 等）
                </font></font></p>
				<p align="center" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
				（也支持STL、OBJ等）
				</font></font></p>
                <a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/solar-seaplane-preliminary-sizing/raw/main/CAD/renders/raytrace-lowres.jpg"><img src="https://github.com/peterdsharpe/solar-seaplane-preliminary-sizing/raw/main/CAD/renders/raytrace-lowres.jpg" alt="计算机辅助设计" style="max-width: 100%;"></a>
            </td>
          <td width="33%" valign="top">
			<p align="center" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
				用户提供的模型+代码（用于定制空气动力学、结构、推进力或其他任何东西 - 例如，用于优化通过概率风场的飞行，如下所示） 
			</font></font></p>
			<a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/AeroSandbox/blob/master/media/images/wind_speeds_model.png"><img src="https://github.com/peterdsharpe/AeroSandbox/raw/master/media/images/wind_speeds_model.png" alt="风速" style="max-width: 100%;"></a>
		</td>
	</tr>
</tbody></table>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或者，完全抛弃所有特定于飞机设计的代码，并使用 AeroSandbox 纯粹作为优化求解器或作为非线性方程组（或 ODE、或 PDE）的求解器：</font></font></p>
<table>
	<tbody><tr>
		<td width="50%" valign="top">
			<p align="center" dir="auto">
				<a href="https://github.com/peterdsharpe/AeroSandbox/blob/develop/tutorial/01%20-%20Optimization%20and%20Math/01%20-%202D%20Rosenbrock.ipynb"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">优化2D Rosenbrock函数</font></font></a>
			</p>
			<a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/AeroSandbox/blob/master/media/images/optimization.png"><img src="https://github.com/peterdsharpe/AeroSandbox/raw/master/media/images/optimization.png" alt="优化" style="max-width: 100%;"></a>
		</td>
		<td width="50%" valign="top">
			<p align="center" dir="auto">
				<a href="https://github.com/peterdsharpe/AeroSandbox/tree/develop/tutorial/03%20-%20Trajectory%20Optimization%20and%20Optimal%20Control/01%20-%20Solving%20ODEs%20with%20AeroSandbox"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指定 Falkner Skan ODE（非线性、三阶 BVP），并让 AeroSandbox 自动处理离散化、求解甚至逆求解。</font></font></a>
			</p>
			<a target="_blank" rel="noopener noreferrer" href="https://github.com/peterdsharpe/AeroSandbox/blob/master/media/images/falkner-skan.png"><img src="https://github.com/peterdsharpe/AeroSandbox/raw/master/media/images/falkner-skan.png" alt="频域常微分方程" style="max-width: 100%;"></a>
		</td>
</tr>
</tbody></table>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">还有很多很多。最重要的是，可以任意组合这些工具，而不会损失优化速度，也不需要任何繁琐的导数数学，这一切都归功于 AeroSandbox 的端到端自动微分能力。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">入门</font></font></h2><a id="user-content-getting-started" class="anchor" aria-label="永久链接：开始使用" href="#getting-started"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font></h3><a id="user-content-installation" class="anchor" aria-label="永久链接：安装" href="#installation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">简而言之：</font></font></p>
<ul dir="auto">
<li>
<p dir="auto"><code>pip install aerosandbox[full]</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">完成安装。</font></font></p>
</li>
<li>
<p dir="auto"><code>pip install aerosandbox</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于具有最小依赖性的轻量级（无头）安装。包括所有优化、数值和物理模型，但跳过可选的可视化依赖项。</font></font></p>
</li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关更多安装详细信息（例如，如果您是 Python 新手），</font></font><a href="/peterdsharpe/AeroSandbox/blob/master/INSTALLATION.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请参阅此处</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">教程、示例和文档</font></font></h3><a id="user-content-tutorials-examples-and-documentation" class="anchor" aria-label="永久链接：教程、示例和文档" href="#tutorials-examples-and-documentation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要开始使用，</font></font><a href="/peterdsharpe/AeroSandbox/blob/master/tutorial"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请查看此处的教程文件夹</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">！所有教程都可以在浏览器中查看，或者您可以通过克隆此存储库将它们作为 Jupyter 笔记本打开。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关 AeroSandbox 的更详细和理论性的介绍，</font></font><a href="/peterdsharpe/AeroSandbox/blob/master/tutorial/sharpe-pds-sm-AeroAstro-2021-thesis.pdf"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请参阅这篇论文</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关 AeroSandbox 模块的更详细的开发人员级别描述，</font></font><a href="/peterdsharpe/AeroSandbox/blob/master/aerosandbox/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请参阅开发人员自述文件</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关完整详细的 API 文档，请参阅</font></font><a href="https://aerosandbox.readthedocs.io/en/master/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档网站</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以使用内置</font></font><code>help()</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">函数（例如</font></font><code>help(asb.Airplane)</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">）打印任何 AeroSandbox 对象的文档和示例。 AeroSandbox 代码也在源代码中进行了</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">广泛的</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">记录，并包含数百个单元测试示例，因此检查源代码也很有用。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用详情</font></font></h3><a id="user-content-usage-details" class="anchor" aria-label="永久链接：使用详情" href="#usage-details"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">单位</font></font></h4><a id="user-content-units" class="anchor" aria-label="永久链接：单位" href="#units"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最后一点需要注意：</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AeroSandbox 的所有输入和输出均以基本 SI 单位或其派生单位表示</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（例如，米、千克、秒、N、m/s、J、Pa）。由于该单位制是</font></font><a href="https://en.wikipedia.org/wiki/Coherence_(units_of_measurement)" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">连贯的</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，</font><font style="vertical-align: inherit;">因此可以在没有任何比例因子的情况下转换</font></font><a href="https://en.wikipedia.org/wiki/SI_derived_unit" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大量的数量</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。这提高了可读性并减少了错误的可能性。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SI 无处不在的规则只有两个例外：</font></font></p>
<ol dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果变量名称的后缀中注明了替代单位。例如：</font></font></p>
<ul dir="auto">
<li><code>battery_capacity</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">→ 焦耳</font></font></li>
<li><code>battery_capacity_watt_hours</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">→ 瓦时</font></font></li>
<li><code>aircraft_endurance</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">→ 秒</font></font></li>
<li><code>aircraft_endurance_hours</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">→ 时间</font></font></li>
</ul>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">根据长期的航空航天惯例，</font><font style="vertical-align: inherit;">攻角 ( </font></font><code>alpha</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">, α) 和侧滑角 ( , β) 以度为单位。</font></font><code>beta</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">所有其他角度和角速率均使用弧度。</font></font></p>
</li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">此外，为了防止函数输入和输出的单位出现任何混淆，所有函数文档字符串中都会列出单位。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您想使用其他单位，请考虑使用</font></font><a href="/peterdsharpe/AeroSandbox/blob/master/aerosandbox/tools/units.py"><code>aerosandbox.tools.units</code></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">轻松转换。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">项目详情</font></font></h2><a id="user-content-project-details" class="anchor" aria-label="永久链接：项目详情" href="#project-details"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">贡献</font></font></h3><a id="user-content-contributing" class="anchor" aria-label="永久链接：贡献" href="#contributing"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请随时加入 AeroSandbox 的开发 - 我们始终欢迎贡献！如果您想要进行更改，最简单的方法是提交拉取请求。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该文本文件</font></font><a href="/peterdsharpe/AeroSandbox/blob/master/CONTRIBUTING.md"><code>CONTRIBUTING.md</code></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为开发人员和高级用户提供了更多详细信息。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您已经添加了一些内容并希望参与更长期的工作，请给我留言！联系信息可以在本自述文件顶部附近我的名字旁边找到。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">捐赠</font></font></h3><a id="user-content-donating" class="anchor" aria-label="永久链接：捐赠" href="#donating"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="https://paypal.me/peterdsharpe" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您喜欢这个软件，请考虑通过 PayPal</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
或</font></font><a href="https://github.com/sponsors/peterdsharpe/"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GitHub 赞助商</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">捐赠以支持开发</font><font style="vertical-align: inherit;">！收益将用于为研究生提供更多咖啡。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">虫子</font></font></h3><a id="user-content-bugs" class="anchor" aria-label="永久链接：错误" href="#bugs"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请通过</font></font><a href="https://github.com/peterdsharpe/AeroSandbox/issues"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">创建新问题</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">来报告所有错误！</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">版本控制</font></font></h3><a id="user-content-versioning" class="anchor" aria-label="永久链接：版本控制" href="#versioning"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AeroSandbox 松散地使用</font></font><a href="https://semver.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">语义版本控制</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，这应该让您了解是否可以从任何给定的更新中获得向后兼容性和/或新功能。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关更多详细信息，请参阅</font></font><a href="/peterdsharpe/AeroSandbox/blob/master/CHANGELOG.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">变更日志</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">引文和商业用途</font></font></h3><a id="user-content-citation--commercial-use" class="anchor" aria-label="永久链接：引文和商业用途" href="#citation--commercial-use"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您发现 AeroSandbox 在研究出版物中有用，请使用以下 BibTeX 片段引用它：</font></font></p>
<div class="highlight highlight-text-bibtex notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-k">@mastersthesis</span>{<span class="pl-en">aerosandbox</span>,
    <span class="pl-s">title</span> = <span class="pl-s"><span class="pl-pds">{</span>AeroSandbox: A Differentiable Framework for Aircraft Design Optimization<span class="pl-pds">}</span></span>,
    <span class="pl-s">author</span> = <span class="pl-s"><span class="pl-pds">{</span>Sharpe, Peter D.<span class="pl-pds">}</span></span>,
    <span class="pl-s">school</span> = <span class="pl-s"><span class="pl-pds">{</span>Massachusetts Institute of Technology<span class="pl-pds">}</span></span>,
    <span class="pl-s">year</span> = <span class="pl-s"><span class="pl-pds">{</span>2021<span class="pl-pds">}</span></span>
}</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="@mastersthesis{aerosandbox,
    title = {AeroSandbox: A Differentiable Framework for Aircraft Design Optimization},
    author = {Sharpe, Peter D.},
    school = {Massachusetts Institute of Technology},
    year = {2021}
}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">商业用户：如果这个包被证明有帮助，我非常乐意讨论主动 AeroSandbox 支持的咨询工作 - 使用本自述文件标题中的电子邮件地址进行联系。</font></font></p>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">执照</font></font></h4><a id="user-content-license" class="anchor" aria-label="永久链接：许可证" href="#license"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a href="/peterdsharpe/AeroSandbox/blob/master/LICENSE.txt"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">麻省理工学院许可证适用，完整条款见此处</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。简而言之：将 AeroSandbox 用于您想要的任何用途（商业或非商业）。 AeroSandbox 发布的目的是希望它有用，但不提供任何适销性保证（无论明示或暗示）。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您使用 AeroSandbox，请注明出处。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">随着时间的推移观星者</font></font></h2><a id="user-content-stargazers-over-time" class="anchor" aria-label="永久链接：随着时间的推移观星者" href="#stargazers-over-time"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a href="https://starchart.cc/peterdsharpe/AeroSandbox" rel="nofollow"><img src="https://camo.githubusercontent.com/63e9e258389a7c73d566ebd109499bddebe8dd0b2e5a2a2ccfd7337308f6e8ad/68747470733a2f2f7374617263686172742e63632f7065746572647368617270652f4165726f53616e64626f782e737667" alt="随着时间的推移观星者" data-canonical-src="https://starchart.cc/peterdsharpe/AeroSandbox.svg" style="max-width: 100%;"></a></p>
</article></div>
